# Touche-Clarifai Matching Results 
| Topic-ID | Title | Amount Unique Labels Touche | Amount Unique Labels Clarifai | Exact Matches | Levenshtein Matches | 
|---|---|---|---|---|---| 
| 100 | Do we need cash? | 731 | 727 | 151 | 220 | 
| 76 | Are video games art? | 717 | 784 | 170 | 249 | 
| 81 | Is genetically modified food unsafe? | 815 | 868 | 202 | 265 | 
| 51 | Do we need sex education in schools? | 624 | 687 | 140 | 194 | 
| 55 | Should agricultural subsidies be reduced? | 646 | 691 | 153 | 218 | 

