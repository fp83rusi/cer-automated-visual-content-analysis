# Exploratory Data Analysis 
| Topic-ID | Title | Amount Images | Average Amount Labels per Image | Minimum Amount Labels per Image | Maximum Amount Labels per Image| Amount Empty Sets | Amount Unique Labels | 
|---|---|---|---|---|---|---|---| 
| 100 | Do we need cash? | 900 | 9.93 | 3 | 10 | 0 | 731 | 
| 76 | Are video games art? | 900 | 9.96 | 3 | 10 | 0 | 717 | 
| 81 | Is genetically modified food unsafe? | 900 | 9.9 | 2 | 10 | 0 | 815 | 
| 51 | Do we need sex education in schools? | 900 | 9.84 | 1 | 10 | 0 | 624 | 
| 55 | Should agricultural subsidies be reduced? | 900 | 9.95 | 4 | 10 | 0 | 646 | 

